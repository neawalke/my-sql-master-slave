#!/bin/bash


tar xf /usr/src/mysql-5.7.34-linux-glibc2.12-x86_64.tar.gz -C /usr/local
ln -s /usr/local/mysql-5.7.34-linux-glibc2.12-x86_64  {{ pillar['mysql_dir'] }}
chown -R mysql.mysql /usr/local/mysql*
echo 'export PATH=/usr/local/mysql/bin:$PATH' > /etc/profile.d/mysql.sh
{{ pillar['mysql_dir'] }}/bin/mysqld --initialize-insecure --user=mysql --datadir=/opt/data
