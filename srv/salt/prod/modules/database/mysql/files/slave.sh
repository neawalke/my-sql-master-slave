#!/bin/bash

mysql=/usr/local/mysql/bin/mysql
user=repl
pass=123
master_ip=192.168.8.131
log_file=$($mysql -u$user -p$pass -h$master_ip -e "show master status" | awk -Fv '|' 'NR==2 {print $1}' | awk '{print $1}')
log_pos=$($mysql -u$user -p$pass -h$master_ip  -e "show master status" | awk -Fv '|' 'NR==2 {print $1}' | awk '{print $2}')

/usr/local/mysql/bin/mysql -uroot -p123 -e "change master to master_host='$localhost',master_user='$user',master_password='$pass',master_log_file='$log_file',master_log_pos=$log_pos; start slave; "
