include:
  - modules.database.mysql.install

master-mysql.service:
  service.running:
    - name: mysqld
    - enable: true
    - reload: true
    - require:
      - cmd: install-mysql

master-set-mysql-passwd:
  cmd.run:
    - name: {{ pillar['mysql_dir'] }}/bin/mysql -e "set password=password('{{ pillar['master-mysql-passwd'] }}');"
    - require:
      - service: master-mysql.service
    - unless: {{ pillar['mysql_dir'] }}/bin/mysql -uroot -p{{ pillar['master-mysql-passwd'] }} -e "exit"

master-slave-dep-packages:
  pkg.installed:
    - pkgs:
      - python3-PyMySQL

create-mysqluser:
  mysql_user.present:
    - name: repl
    - host: 192.168.8.0/255.255.255.0
    - password: {{ pillar['master-user-pass'] }}


grant-mysql:
  mysql_grants.present:
    - grant: replication slave,super
    - database: '*.*'
    - user: repl
    - host: 192.168.8.0/255.255.255.0

update-masterfile:
  file.append:
    - name: /etc/my.cnf
    - text: |
        log-bin = mysql_bin
        server-id = 10
    - require:
      - mysql_grants: grant-mysql

stop-master-mysql:
  service.dead:
    - name: mysqld

start-master-mysql:
  service.running:
    - name: mysqld

