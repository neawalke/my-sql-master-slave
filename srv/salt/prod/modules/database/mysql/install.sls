mysql:
  user.present:
    - shell: /sbin/nologin
    - system: true
    - createhome: false

{% if grains['osmajorrelease'] == 8%}
mysql-dep-package:
  pkg.installed:
    - name: ncurses-compat-libs
{% endif %}

{% if grains['osmajorrelease'] == 7%}
libaio-devel:
  pkg.installed
{% endif %}

/usr/src/mysql-5.7.34-linux-glibc2.12-x86_64.tar.gz:
  file.managed:
    - source: salt://modules/database/mysql/files/mysql-5.7.34-linux-glibc2.12-x86_64.tar.gz


/opt/data:
  file.directory:
    - user: mysql
    - group: mysql
    - mode: '755'

install-mysql:
  cmd.script:
    - name: salt://modules/database/mysql/files/install.sh
    - unless: test -d /usr/local/mysql
    - template: jinja

copy-mysql-soft:
  file.managed:
    - names:
      - /usr/lib/systemd/system/mysqld.service:
        - source: salt://modules/database/mysql/files/mysqld.service
        - user: root
        - group: root
        - mode: 644
        - template: jinja
      - {{ pillar['mysql_dir'] }}/support-files/mysql.server:
        - source: salt://modules/database/mysql/files/mysql.server
        - user: mysql
        - group: mysql
        - mode: 755
        - template: jinja
      - /etc/my.cnf:
        - source: salt://modules/database/mysql/files/my.cnf
        - user: root
        - group: root
        - mode: 644
        - template: jinja
    - require:
      - cmd: install-mysql
