include:
  - modules.database.mysql.install

slave-mysql.service:
  service.running:
    - name: mysqld
    - enable: true
    - reload: true
    - require:
      - cmd: install-mysql


salve-set-mysql-passwd:
  cmd.run:
    - name: {{ pillar['mysql_dir'] }}/bin/mysql -e "set password=password('{{ pillar['slave-mysql-passwd'] }}');"
    - require:
      - service: mysqld
    - unless: {{ pillar['mysql_dir'] }}/bin/mysql -uroot -p{{ pillar['slave-mysql-passwd'] }} -e "exit"

update-slave-mysql-file:
  file.append:
    - name: /etc/my.cnf
    - text: |
        relay-log = mysql_relay-bin
        server-id = 20
   
stop-slave-mysql:
  service.dead:
    - name: mysqld
    - require:
      - file: update-slave-mysql-file

start-slave-mysql:
  service.running:
    - name: mysqld

project-slave-scripts:
  cmd.script:
    - name: salt://modules/database/sub/files/slave.sh
